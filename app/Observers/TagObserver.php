<?php

namespace App\Observers;

use App\Tag;

class TagObserver
{
    /**
     * @param Tag $tag
     */
    public function saving(Tag $tag)
    {
        $slug_stub = str_slug($tag->title);
        $slug = $slug_stub;
        $index = 0;

        while (Tag::bySlug($slug_stub)->exists()) {
            $index++;
            $slug = $slug_stub . '-' . $index;
        }

        $tag->slug = $slug;
    }
}