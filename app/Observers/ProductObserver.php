<?php

namespace App\Observers;

use App\Product;

class ProductObserver
{
    /**
     * @param Product $product
     */
    public function saving(Product $product)
    {
        $slug_stub = str_slug($product->title);
        $slug = $slug_stub;
        $index = 0;

        while (Product::bySlug($slug_stub)->exists()) {
            $index++;
            $slug = $slug_stub . '-' . $index;
        }

        $product->slug = $slug;
    }
}