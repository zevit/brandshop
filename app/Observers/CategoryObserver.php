<?php

namespace App\Observers;

use App\Category;

class CategoryObserver
{
    /**
     * @param Category $category
     */
    public function saving(Category $category)
    {
        $slug_stub = str_slug($category->name);

        if ($category->parent) {
            $slug_stub = $category->parent->slug . '-' . $slug_stub;
        }

        $slug = $slug_stub;
        $index = 0;

        while (Category::bySlug($slug)->exists()) {
            $index++;
            $slug = $slug_stub . '-' . $index;
        }

        $category->slug = $slug;
    }
}