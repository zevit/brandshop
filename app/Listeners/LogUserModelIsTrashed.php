<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticatedModelTrashed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogUserModelIsTrashed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticatedModelTrashed  $event
     * @return void
     */
    public function handle(AuthenticatedModelTrashed $event)
    {
        \Log::info('UserModelIsTrashed', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
