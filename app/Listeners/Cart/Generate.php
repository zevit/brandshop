<?php

namespace App\Listeners\Cart;

use Gloudemans\Shoppingcart\Facades\Cart;
use App\Events\Auth\Registered;

class Generate
{
    /**
     * Handle the event.
     *
     * @param  Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        Cart::store($event->user->id);
    }
}
