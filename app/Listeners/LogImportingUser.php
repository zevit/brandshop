<?php

namespace App\Listeners;

use Adldap\Laravel\Events\Importing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogImportingUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Importing  $event
     * @return void
     */
    public function handle(Importing $event)
    {
        \Log::info('ImportingUser', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
