<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticatedWithCredentials;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthWithCredentials
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticatedWithCredentials  $event
     * @return void
     */
    public function handle(AuthenticatedWithCredentials $event)
    {
        \Log::info('AuthWithCredentials', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
