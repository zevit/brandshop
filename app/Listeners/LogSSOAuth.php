<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticatedWithWindows;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSSOAuth
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticatedWithWindows  $event
     * @return void
     */
    public function handle(AuthenticatedWithWindows $event)
    {
        \Log::info('SSOAuth', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
