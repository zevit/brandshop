<?php

namespace App\Listeners;

use Adldap\Laravel\Events\Synchronizing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSynchronizingUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Synchronizing  $event
     * @return void
     */
    public function handle(Synchronizing $event)
    {
        \Log::info('SynchronizingUser', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
