<?php

namespace App\Listeners;

use Adldap\Laravel\Events\Authenticating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthenticating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Authenticating $event
     * @return void
     */
    public function handle(Authenticating $event)
    {
        \Log::info('Authenticating', [
            'username' => $event->username,
            'user' => $event->user,
        ]);
    }
}
