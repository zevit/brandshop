<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticationRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthRejected
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticationRejected $event
     * @return void
     */
    public function handle(AuthenticationRejected $event)
    {
        \Log::info('AuthRejected', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
