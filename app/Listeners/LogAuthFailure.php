<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticationFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthFailure
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticationFailed $event
     * @return void
     */
    public function handle(AuthenticationFailed $event)
    {
        \Log::info('AuthFailure', [
            'user' => $event->user,
        ]);
    }
}
