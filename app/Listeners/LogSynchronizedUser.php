<?php

namespace App\Listeners;

use Adldap\Laravel\Events\Synchronized;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSynchronizedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Synchronized  $event
     * @return void
     */
    public function handle(Synchronized $event)
    {
        \Log::info('SynchronizedUser', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
