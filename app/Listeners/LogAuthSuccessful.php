<?php

namespace App\Listeners;

use Adldap\Laravel\Events\AuthenticationSuccessful;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthSuccessful
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticationSuccessful $event
     * @return void
     */
    public function handle(AuthenticationSuccessful $event)
    {
        \Log::info('AuthSuccessful', [
            'user' => $event->user,
            'model' => $event->model,
        ]);
    }
}
