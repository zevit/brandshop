<?php

namespace App\Listeners;

use Adldap\Laravel\Events\DiscoveredWithCredentials;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogAuthUserLocated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DiscoveredWithCredentials  $event
     * @return void
     */
    public function handle(DiscoveredWithCredentials $event)
    {
        \Log::info('AuthUserLocated', [
            'user' => $event->user,
        ]);
    }
}
