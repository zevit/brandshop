<?php

namespace App\Providers;

use App\Http\ViewComposers\ManufacturersComposer;
use App\Http\ViewComposers\NavigationComposer;
use App\Http\ViewComposers\TagsComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'layouts.partials._navigation',
            'admin.products.filter',
            'admin.products.form',
        ], NavigationComposer::class);

        View::composer([
            'admin.products.filter',
            'products.index'
        ], ManufacturersComposer::class);

        View::composer([
            'admin.products.form',
        ], TagsComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
