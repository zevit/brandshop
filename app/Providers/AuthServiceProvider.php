<?php

namespace App\Providers;

use App\Extensions\SSOUserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Category' => 'App\Policies\CategoryPolicy',
        'App\Slider' => 'App\Policies\SliderPolicy',
        'App\Product' => 'App\Policies\ProductPolicy',
        'App\Transaction' => 'App\Policies\TransactionPolicy',
        'App\Purchase' => 'App\Policies\PurchasePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('sso', function ($app, array $config) {
            return new SSOUserProvider($app, $config);
        });
    }
}
