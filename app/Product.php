<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Gloudemans\Shoppingcart\CanBeBought;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use File;

class Product extends Model
{
    use CanBeBought;

    const IMAGE_WIDTH = 430;
    const IMAGE_HEIGHT = 300;
    const POSTER_WIDTH = 240;
    const POSTER_HEIGHT = 170;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'description',
        'price',
        'provider',
        'manufacturer',
        'poster',
        'images',
        'colors',
        'sizes',
        'quantity',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'float',
        'images' => 'array',
        'colors' => 'array',
        'sizes' => 'array',
        'quantity' => 'integer',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
//    protected $appends = ['rating', 'poster_path', 'image_paths'];
    protected $appends = ['poster_path', 'image_paths'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * The tags that is associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Get all the rates from the users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get average rating from all of the rates.
     *
     * @return mixed
     */
//    public function getRatingAttribute()
//    {
//        return $this->ratings()->avg('rating');
//    }

    /**
     * @return null|string
     */
    public function getPosterPathAttribute()
    {
        if (!$this->hasPoster()) {
            return null;
        }

        return "/storage/products/{$this->poster}.png";
    }

    /**
     * @return null|array
     */
    public function getImagePathsAttribute()
    {
        if (!$this->hasImages()) {
            return null;
        }

        $images = [];

        foreach ($this->images as $image) {
            $images[] = "/storage/products/{$image}.png";

        }

        return $images;
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return $this
     */
    public function scopeByTitle(Builder $builder, $value)
    {
        return $builder->where('title', 'like', "%{$value}%");
    }

    /**
     * @param Builder $builder
     * @param $slug
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeBySlug(Builder $builder, $slug)
    {
        return $builder->where('slug', $slug);
    }

    /**
     * @param Builder $builder
     * @param $value
     * @param bool $strict
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeByCategory(Builder $builder, $value, $strict = true)
    {
        $children = [];

        if (!$strict) {
            $category = Category::findOrFail($value);

            if ($category->isParent()) {
                $category->load('children');
                $children = $category->children->pluck('id')->toArray();
            }
        }

        return $builder->whereHas('categories', function (Builder $query) use ($children, $value) {
            $query->whereIn('category_id', array_merge($children, [$value]));
        });
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return $this
     */
    public function scopeByRating(Builder $builder, $value)
    {
        // TODO
        return $builder->where('title', 'like', "%{$value}%");
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return $this
     */
    public function scopeByManufacturer(Builder $builder, $value)
    {
        return $builder->where('manufacturer', $value);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeByPopularity(Builder $builder)
    {
        return $builder->orderBy('purchases', 'desc');
    }

    /**
     * @param Builder $builder
     * @param array $filters
     * @return Builder
     */
    public function scopeFilter(Builder $builder, $filters = [])
    {
        $scopes = [];

        foreach ($filters as $filter => $value) {
            if ($value !== 'all' && !empty($value)) {
                $scope = ucfirst(Str::camel(str_replace('_id', '', $filter)));

                if ($scope === 'Category') {
                    $scopes["by{$scope}"] = [$value, false];
                } else {
                    $scopes["by{$scope}"] = $value;
                }
            }
        }

        return $builder->scopes($scopes);
    }

    /**
     * @return bool
     */
    public function hasPoster()
    {
        return $this->poster;
    }

    /**
     * @return bool
     */
    public function hasImages()
    {
        return $this->images !== null && count($this->images) > 0;
    }

    /**
     * @param Request $request
     */
    public function uploadPoster(Request $request)
    {
        if (!$request->has('poster')) {
            return;
        }

        $image = Image::make($request->file('poster'));
        $image->fit(static::POSTER_WIDTH, static::POSTER_HEIGHT, function (Constraint $constraint) {
            $constraint->upsize();
        });
        $image->encode('png');

        $hash = md5($image->__toString());
        Storage::disk('public')->put("products/{$hash}.png", $image->__toString());

        $this->poster = $hash;
    }

    /**
     * @param Request $request
     */
    public function uploadImages(Request $request)
    {
        if (!$request->has('images')) {
            return;
        }

        $images = [];

        foreach ($request->file('images') as $file) {
            $image = Image::make($file);
            $image->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT, function (Constraint $constraint) {
                $constraint->upsize();
            });
            $image->encode('png');

            $hash = md5($image->__toString());
            Storage::disk('public')->put("products/{$hash}.png", $image->__toString());

            $images[] = $hash;
        }

        $this->images = $images;
    }

    /**
     * @return $this
     */
    public function deleteImages()
    {
        $images = [
            public_path("products/{$this->poster}.png"),
        ];

        if ($this->hasImages()) {
            foreach ($this->images as $image) {
                $images[] = public_path("products/{$image}.png");

            }
        }

        Storage::disk('public')->delete($images);

        return $this;
    }
}
