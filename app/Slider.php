<?php

namespace App;

use File;
use Image;
use Illuminate\Http\Request;
use Intervention\Image\Constraint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Slider extends Model
{
    const IMAGE_WIDTH = 1300;
    const IMAGE_HEIGHT = 558;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['image', 'link', 'priority'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_path'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'priority' => 'integer',
    ];

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        if (!$this->hasImage()) {
            return null;
        }

        return "/storage/slides/{$this->image}.png";
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeByPriority(Builder $builder)
    {
        return $builder->orderBy('priority');
    }

    /**
     * @return bool
     */
    public function hasLink()
    {
        return $this->link !== null;
    }

    /**
     * @return bool
     */
    public function hasImage()
    {
        return $this->image !== null;
    }

    /**
     * @param Request $request
     */
    public function uploadImage(Request $request)
    {
        if (!$request->has('image')) {
            return;
        }

        $hash = hash('sha256', $request->file('image') . time());
        $image = Image::make($request->file('image'));

        $image->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT, function (Constraint $constraint) {
            $constraint->upsize();
        });

        $image->save("storage/slides/{$hash}.png");

        $this->image = $hash;
    }

    /**
     * @return static
     */
    public function deleteImage()
    {
        $image = public_path("storage/slides/{$this->image}.png");

        File::delete($image);

        return $this;
    }
}
