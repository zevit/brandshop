<?php

namespace App;

use File;
use Image;
use Illuminate\Http\Request;
use Intervention\Image\Constraint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{
    const IMAGE_WIDTH = 333;
    const IMAGE_HEIGHT = 226;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'name', 'slug', 'description', 'image', 'priority', 'is_featured'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_path'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'parent_id' => 'integer',
        'is_featured' => 'boolean',
        'priority' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        if (!$this->hasImage()) {
            return null;
        }

        return "/storage/categories/{$this->image}.png";
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeParents(Builder $builder)
    {
        return $builder->whereNull('parent_id');
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeFeatured(Builder $builder)
    {
        return $builder->where('is_featured', true);
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeByName(Builder $builder)
    {
        return $builder->orderBy('name');
    }

    /**
     * @param Builder $builder
     * @param $slug
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeBySlug(Builder $builder, $slug)
    {
        return $builder->where('slug', $slug);
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function scopeByPriority(Builder $builder)
    {
        return $builder->orderBy('priority');
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        return !$this->parent_id;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children()->exists();
    }

    /**
     * @return bool
     */
    public function hasImage()
    {
        return $this->image !== null;
    }

    /**
     * @param Request $request
     */
    public function uploadImage(Request $request)
    {
        if (!$request->has('image')) {
            return;
        }

        $hash = hash('sha256', $request->file('image') . time());
        $image = Image::make($request->file('image'));

        $image->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT, function (Constraint $constraint) {
            $constraint->upsize();
        });

        $image->save("storage/categories/{$hash}.png");

        $this->image = $hash;
    }

    /**
     * @return static
     */
    public function deleteImage()
    {
        $image = public_path("storage/categories/{$this->image}.png");

        File::delete($image);

        return $this;
    }
}
