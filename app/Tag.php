<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug'];

    /**
     * The products that is associated with the tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return $this
     */
    public function scopeBySlug(Builder $builder, $value)
    {
        return $builder->where('slug', $value);
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return $this
     */
    public function scopeByTitle(Builder $builder, $value)
    {
        return $builder->where('title', $value);
    }
}
