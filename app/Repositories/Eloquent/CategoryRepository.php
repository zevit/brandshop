<?php

namespace App\Repositories\Eloquent;

use App\Category;

class CategoryRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Category::class;
    }
}