<?php

namespace App\Queries;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class ProductByRequestQuery
{
    /**
     * @param Request $request
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function handle(Request $request, $perPage = 15)
    {
        $categories = explode(',', $request->categories);
        $brands = explode(',', $request->brands);
        $prices = explode(',', $request->prices);

        $query = Product::leftJoin('category_product', 'category_product.product_id', 'products.id')
            ->leftJoin('categories', 'category_product.category_id', 'categories.id');

        if ($request->has('categories') && count($categories) >= 1) {
            $query->whereIn('categories.slug', $categories);
        }

        if ($request->has('brands') && count($brands) >= 1) {
            $query->whereIn('products.manufacturer', $brands);
        }

        if ($request->has('prices')) {
            $query->where(function (Builder $q) use ($prices) {
                foreach ($prices as $priceRange) {
                    $range = explode('-', $priceRange);

                    $q->orWhereBetween('products.price', $range);
                }
            });
        }

        if ($request->has('search')) {
            $query->where(function (Builder $q) use ($request) {
                $q->where('products.title', 'LIKE', "%{$request->search}%");
                $q->orWhere('products.description', 'LIKE', "%{$request->search}%");
            });
        }

        return $query->orderBy('products.created_at', 'desc')->paginate($perPage);
    }
}