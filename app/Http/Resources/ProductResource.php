<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'poster' => $this->when($this->hasPoster(), $this->imageTag($this->id, $this->poster_path)),
            'images' => $this->when($this->hasImages(), $this->imageTag($this->id, $this->image_paths)),
            'link' => route('product', $this),
            'price' => $this->price . ' ₾',
            'quantity' => $this->quantity,
        ];
    }

    /**
     * @param $id
     * @param $paths
     * @return string
     */
    protected function imageTag($id, $paths)
    {
        if (is_array($paths)) {
            $img = '';

            foreach ($paths as $path) {
                $img .= '<img src="' . asset($path) . '" alt="Product #' . $id . '" class="js-zoom" height="100">';
            }

            return $img;
        }

        return '<img src="' . asset($paths) . '" alt="Product #' . $id . '" class="js-zoom" height="100">';
    }
}
