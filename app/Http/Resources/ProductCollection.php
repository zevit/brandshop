<?php

namespace App\Http\Resources;

use App\Product;
use App\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ProductResource::collection($this->collection),
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        $brands = Product::pluck('manufacturer', 'manufacturer')->unique()->reject(function ($name) {
            return empty($name);
        });

        if ($request->has('category')) {
            $categories = Category::bySlug($request->category)->first()->children()->byPriority()->pluck('name', 'slug');
        } else {
            $categories = Category::parents()->byPriority()->pluck('name', 'slug');
        }

        return [
            'meta' => [
                'categories' => $categories,
                'brands' => $brands,
            ],
        ];
    }
}
