<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Queries\ProductByRequestQuery;

class CategoryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Category $category
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Request $request)
    {
        if ($request->has('categories') || $request->has('brands') || $request->has('prices')) {
            $products = ProductByRequestQuery::handle($request);
        } else {
            $products = $category->products()->latest()->paginate(1);
        }

        if ($category->isParent()) {
            $categories = $category->children()->byPriority()->get();
        } else {
            $categories = $category->parent->children()->byPriority()->get();
        }

        return view('products.index', compact('category', 'categories', 'products'));
    }
}
