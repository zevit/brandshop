<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Tag;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $request->only(['title', 'rating', 'manufacturer', 'category_id', 'sub_category_id']);
        $products = Product::with('categories')->filter($filters)->latest()->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product;

        return view('admin.products.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $product = new Product;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->provider = $request->provider;
        $product->manufacturer = $request->manufacturer;
        $product->quantity = $request->quantity;
        $product->colors = explode('|', $request->colors);
        $product->sizes = explode('|', $request->sizes);

        $product->uploadPoster($request);
        $product->uploadImages($request);

        $tags = [];

        foreach (explode('|', $request->tags) as $tag) {
            if (!Tag::byTitle($tag)->exists()) {
                $tags[] = new Tag(['title' => $tag]);
            }
        }

        if ($product->save()) {
            $product->categories()->sync($request->categories_id);
            $product->tags()->saveMany($tags);

            session()->flash('success', 'პროდუქტი დაემატა');
        } else {
            session()->flash('success', 'პროდუქტი ვერ დაემატა');
        }

        return redirect()->route('products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load('categories');

        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->provider = $request->provider;
        $product->manufacturer = $request->manufacturer;
        $product->quantity = $request->quantity;
        $product->colors = explode('|', $request->colors);
        $product->sizes = explode('|', $request->sizes);

        if ($request->has('poster')) {
            $product->uploadPoster($request);
        }

        if ($request->has('images')) {
            $product->uploadImages($request);
        }

        $tags = [];

        foreach (explode('|', $request->tags) as $tag) {
            if (!Tag::byTitle($tag)->exists()) {
                $tags[] = new Tag(['title' => $tag]);
            }
        }

        if ($product->save()) {
            $product->categories()->sync($request->categories_id);
            $product->tags()->detach();

//            tap($product->tags(), function ($t) use ($tags) {
//                $t->detach();
//                $t->saveMany($tags);
//            });

                session()->flash('success', 'პროდუქტი შეიცვალა');
        } else {
            session()->flash('success', 'პროდუქტი ვერ შეიცვალა');
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        if ($product->deleteImages()->delete()) {
            session()->flash('success', 'პროდუქტი წაიშალა');
        } else {
            session()->flash('success', 'პროდუქტი ვერ წაიშალა');
        }

        return back();
    }
}
