<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::parents()->with('children')->byPriority()->latest()->paginate(10);

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $category = new Category;
        $categories = Category::parents()->byName()->get();

        return view('admin.categories.create', compact('categories', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCategoryRequest $request)
    {
        $category = new Category;
        $category->parent_id = $request->parent_id;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->is_featured = $request->has('is_featured');
        $category->priority = $request->priority;

        $category->uploadImage($request);

        if ($category->save()) {
            session()->flash('success', 'კატეგორია დაემატა');
        } else {
            session()->flash('success', 'კატეგორია ვერ დაემატა');
        }

        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::parents()->byName()->get();

        return view('admin.categories.edit', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryRequest $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->parent_id = $request->parent_id;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->is_featured = $request->has('is_featured');
        $category->priority = $request->priority;

        if ($request->has('image')) {
            $category->uploadImage($request);
        }

        if ($category->save()) {
            session()->flash('success', 'კატეგორია შეიცვალა');
        } else {
            session()->flash('success', 'კატეგორია ვერ შეიცვალა');
        }

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->children()->each(function (Category $cat) {
            $cat->deleteImage()->delete();
        });

        if ($category->deleteImage()->delete()) {
            session()->flash('success', 'კატეგორია წაიშალა');
        } else {
            session()->flash('success', 'კატეგორია ვერ წაიშალა');
        }

        return back();
    }
}
