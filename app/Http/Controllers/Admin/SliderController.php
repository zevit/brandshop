<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSliderRequest;
use App\Http\Requests\UpdateSliderRequest;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::byPriority()->latest()->paginate(10);

        return view('admin.slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slider = new Slider;

        return view('admin.slider.create', compact('slider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateSliderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSliderRequest $request)
    {
        $slider = new Slider;
        $slider->link = $request->link;
        $slider->priority = $request->priority;

        $slider->uploadImage($request);

        if ($slider->save()) {
            session()->flash('success', 'სლაიდი დაემატა');
        } else {
            session()->flash('success', 'სლაიდი ვერ დაემატა');
        }

        return redirect()->route('slider.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSliderRequest $request
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSliderRequest $request, Slider $slider)
    {
        $slider->link = $request->link;
        $slider->priority = $request->priority;

        if ($request->has('image')) {
            $slider->uploadImage($request);
        }

        if ($slider->save()) {
            session()->flash('success', 'სლაიდი შეიცვალა');
        } else {
            session()->flash('success', 'სლაიდი ვერ შეიცვალა');
        }

        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider $slider
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Slider $slider)
    {
        if ($slider->deleteImage()->delete()) {
            session()->flash('success', 'სლაიდი წაიშალა');
        } else {
            session()->flash('success', 'სლაიდი ვერ წაიშალა');
        }

        return back();
    }
}
