<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Slider;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slider::byPriority()->get();
        $popular = Product::byPopularity()->take(10)->get();
        $featured = Category::featured()->take(3)->get();
        $latest = Product::latest()->take(12)->get();

        return view('home', compact('popular', 'slides', 'featured', 'latest'));
    }
}
