<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ProductByRequestQuery;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ProductCollection
     */
    public function index(Request $request)
    {
        $perPage = 12;

        if ($request->hasAny('search', 'categories', 'brands', 'rating', 'prices')) {
            $products = ProductByRequestQuery::handle($request, $perPage);
        } elseif ($request->has('category')) {
            $products = Category::bySlug($request->category)->first()->products()->paginate($perPage);
        } else {
            $products = Product::latest()->take($perPage)->paginate($perPage);
        }

        return new ProductCollection($products);
    }
}
