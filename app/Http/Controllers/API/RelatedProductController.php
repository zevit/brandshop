<?php

namespace App\Http\Controllers\API;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;

class RelatedProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $products = Product::byCategory($request->category)->latest()->take(3)->get();

        return ProductResource::collection(
            $products->merge(Product::byCategory($request->category, false)->latest()->take(2)->get())
        );
    }
}
