<?php

namespace App\Http\Controllers\API;

use App\Slider;
use App\Http\Controllers\Controller;
use App\Http\Resources\SlideResource;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return SlideResource::collection(
            Slider::byPriority()->latest()->get()
        );
    }
}
