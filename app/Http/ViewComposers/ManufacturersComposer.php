<?php

namespace App\Http\ViewComposers;

use App\Product;
use Illuminate\View\View;

class ManufacturersComposer
{
    /**
     * The category model implementation.
     *
     * @var Product
     */
    protected $product;

    /**
     * NavigationComposer constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('manufacturers', $this->product->pluck('manufacturer')->unique()->reject(function ($name) {
            return empty($name);
        }));
    }
}