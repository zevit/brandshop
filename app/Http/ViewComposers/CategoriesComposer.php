<?php

namespace App\Http\ViewComposers;

use App\Category;
use Illuminate\View\View;

class CategoriesComposer
{
    /**
     * The category model implementation.
     *
     * @var Category
     */
    protected $category;

    /**
     * NavigationComposer constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->category->byPriority()->get());
    }
}