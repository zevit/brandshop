<?php

namespace App\Http\ViewComposers;

use App\Tag;
use Illuminate\View\View;

class TagsComposer
{
    /**
     * The category model implementation.
     *
     * @var Tag
     */
    protected $tag;

    /**
     * TagsComposer constructor.
     *
     * @param Tag $tag
     */
    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('tags', $this->tag->get());
    }
}