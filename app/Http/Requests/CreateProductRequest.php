<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categories_id' => 'required',
            'title' => 'required|max:255',
            'price' => 'required|numeric',
            'provider' => 'nullable|max:50',
            'manufacturer' => 'nullable|max:50',
            'quantity' => 'required|numeric|min:1',
            'poster' => 'nullable|image',
            'images.*' => 'nullable|image',
        ];
    }
}
