<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $user = \App\User::create([
            'name' => 'Zura Gabievi',
            'email' => 'zura.gabievi@gmail.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);

        if ($user) {
            event(new \App\Events\Auth\Registered($user));
        }

        factory(\App\User::class, 10)->create();
    }
}
