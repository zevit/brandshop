<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();

        factory(\App\Product::class, 50)->create()->each(function (\App\Product $product) {
            $product->tags()->attach(rand(1, 20));
            $product->categories()->attach(rand(1, 40));
        });

    }
}
