<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('categories')->truncate();

        factory(\App\Category::class, 10)->create()->each(function (\App\Category $category) {
            $category->children()->saveMany(factory(\App\Category::class, 3)->make());
        });
    }
}
