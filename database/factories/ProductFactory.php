<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    $title = $faker->unique()->words(3, true);

    return [
        'title' => $title,
        'slug' => str_slug($title),
        'description' => $faker->optional()->sentences(2, true),
        'price' => $faker->randomFloat(2, 5, 5000),
        'provider' => $faker->optional()->word,
        'manufacturer' => $faker->optional()->word,
        'poster' => strtoupper(str_random(64)),
        'quantity' => rand(0, 500),
    ];
});
