<?php

use Faker\Generator as Faker;

$factory->define(App\Tag::class, function (Faker $faker) {
    return [
        'title' => $tag = $faker->unique()->word,
        'slug' => $tag,
    ];
});
