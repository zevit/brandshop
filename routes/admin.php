<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your Dashboard!
|
*/

Route::redirect('/', '/admin/categories')->name('dashboard');
//Route::get('/', 'DashboardController@index')->name('dashboard');

Route::resource('categories', 'CategoryController')->except(['show']);
Route::resource('slider', 'SliderController')->except(['show']);
Route::resource('products', 'ProductController')->except(['show']);
