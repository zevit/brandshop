<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/categories', 'CategoryController@index')->name('api.categories');
Route::get('/slides', 'SliderController@index')->name('api.slides');
Route::get('/featured-categories', 'FeaturedCategoryController@index')->name('api.featured-categories');
Route::get('/popular-products', 'PopularProductController@index')->name('api.popular-products');
Route::get('/related-products', 'RelatedProductController@index')->name('api.related-products');
Route::get('/products', 'ProductController@index')->name('api.products');
