<?php

use Adldap\Laravel\Facades\Adldap;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('users', function() {
    $users = Adldap::search()->users()->find('jbokuchava');

    return $users;
});

Route::get('/', 'HomeController@index')->name('home');
Route::view('/rules', 'rules')->name('rules');
Route::view('/partnership', 'partnership')->name('partnership');

// Login
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

/**
 * /category/{slug}?filter={BASE64_FILTER}
 *
 * {
 *   "categories": [1, 6],
 *   "brands": [3, 8],
 *   "rating": 3.5
 * }
 */
Route::get('/category/{category}', 'CategoryController@show')->name('category');

/**
 * /products?filter={BASE64_FILTER}
 *
 * {
 *   "query": 'სმოკინგი',
 *   "categories": [1, 6],
 *   "brands": [3, 8],
 *   "rating": 3.5
 * }
 */
Route::get('/products', 'ProductController@index')->name('products');

// /product/{id}
Route::get('/product/{product}', 'ProductController@show')->name('product');

// /cart?page=current | /cart?page=history
Route::get('/cart', 'CartController@index')->name('cart');

// /page/{slug}
//Route::get('/page/{page}', 'PageController@show')->name('page');

// /payment
Route::get('/payment/check', 'PaymentController@check')->name('payment.check');
Route::get('/payment/init', 'PaymentController@init')->name('payment.init');
