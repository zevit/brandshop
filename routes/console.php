<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('ide-helper', function () {
    $this->call('ide-helper:generate');
    $this->call('ide-helper:models', ['--nowrite' => true]);
    $this->call('ide-helper:meta');
})->describe('All IDE Helper commands combined');
