<v-touch id="login-popup" class="c-popup c-popup--auth is-hidden" @tap="closeLogin">
    <div class="c-popup__content">
        <form action="{{ route('login') }}" method="post">
            @csrf

            @if($errors->has('email'))
                <p class="u-text--danger">{{ $errors->first('email') }}</p>
            @elseif($errors->has('password'))
                <p class="u-text--danger">{{ $errors->first('password') }}</p>
            @endif

            <input type="text" name="email" class="o-input c-input" placeholder="მომხმარებელი">
            <input type="password" name="password" class="o-input c-input" placeholder="პაროლი">

            <button class="o-button o-button--fluid c-button c-button--primary c-button--filled u-text--uppercase">შესვლა</button>
        </form>
    </div>
    <!-- /.c-popup__content -->
</v-touch>
<!-- /.c-popup -->