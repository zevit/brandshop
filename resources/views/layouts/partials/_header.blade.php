<header class="c-header">
    <div class="o-wrapper">
        <a href="{{ url('/') }}" class="c-logo">Bank of Georgia</a>

        <div class="c-header__controls">
            <search placeholder="პროდუქტის ძიება"></search>

            @auth
                <div class="c-header__user">
                    <a href="#">
                        <img src="{{ auth()->user()->avatar }}" alt="{{ auth()->user()->name }}" width="48" height="48">
                        <h3 class="u-text--uppercase">{{ auth()->user()->name }}</h3>
                    </a>
                </div>

                <form action="{{ route('logout') }}" method="post">
                    @csrf

                    <button class="o-button o-button--icon c-button" style="margin-right: 0.5em;">
                        <i class="c-icon c-icon--lock-open"></i>
                    </button>
                </form>
            @else
                <button class="o-button c-button c-button--primary c-button--auth u-text--uppercase" @click="showLogin">
                    <i class="c-icon c-icon--user"></i><span>ავტორიზაცია</span>
                </button>
            @endauth

            <button class="o-button o-button--icon c-button">
                <i class="c-icon c-icon--cart"></i>
            </button>
            <!-- /.c-button -->

            {{--<button class="o-button o-button--icon c-button is-filled">--}}
                {{--<i class="c-icon c-icon--cart"></i>--}}
                {{--<span class="c-button__badge">2</span>--}}
            {{--</button>--}}
        </div>
        <!-- /.c-header__controls -->
    </div>
    <!-- /.o-wrapper -->
</header>
<!-- /.c-header -->