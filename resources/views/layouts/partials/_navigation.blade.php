<nav class="c-navigation">
    <div class="o-wrapper">
        <ul class="c-navigation__list">
            @foreach($categories as $category)
                <li class="c-navigation__item {{ $category->hasChildren() ? 'has-dropdown' : '' }}">
                    <a href="{{ route('category', $category) }}" class="c-navigation__link u-text--uppercase">{{ $category->name }}</a>

                    @if($category->hasChildren())
                        <div class="c-dropdown">
                            <div class="o-wrapper">
                                <ul class="c-dropdown__list">
                                    @foreach($category->children as $child)
                                        <li class="c-dropdown__item">
                                            <a href="{{ route('category', $child) }}" class="c-dropdown__link">{{ $child->name }}</a>
                                        </li>
                                        <!-- /.c-dropdown__item -->
                                    @endforeach
                                </ul>
                                <!-- /.c-dropdown__list -->

                                <div class="c-dropdown__info">
                                    <h2 class="c-dropdown__title u-text--uppercase">{{ $category->name }}</h2>
                                    <p class="c-dropdown__description">{{ $category->description }}</p>
                                    <a href="{{ route('category', $category) }}" class="c-dropdown__view">ყველას ნახვა</a>
                                </div>
                                <!-- /.c-dropdown__info -->
                            </div>
                            <!-- /.o-wrapper -->
                        </div>
                        <!-- /.c-dropdown -->
                    @endif
                </li>
                <!-- /.c-navigation__item -->
            @endforeach
        </ul>
        <!-- /.c-navigation__list -->
    </div>
    <!-- /.o-wrapper -->
</nav>
<!-- /.c-navigation -->