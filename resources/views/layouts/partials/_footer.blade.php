<footer class="c-footer">
    <div class="o-wrapper">
        <a href="{{ url('/') }}" class="c-logo">Bank of Georgia</a>

        <ul class="c-footer__list">
            <li class="c-footer__item">
                <a href="{{ route('rules') }}" class="c-footer__link u-text--uppercase">წესები<span> და პირობები</span></a>
            </li>
            <!-- /.c-footer__item -->

            <li class="c-footer__item">
                <a href="{{ route('partnership') }}" class="c-footer__link u-text--uppercase">პარტნიორები<span>ს შესახებ</span></a>
            </li>
            <!-- /.c-footer__item -->

            <li class="c-footer__item">
                <a href="tel:0322444444" class="c-footer__link u-text--uppercase">(032) 2 444 444</a>
            </li>
            <!-- /.c-footer__item -->
        </ul>
        <!-- /.c-footer__list -->

        <div class="c-footer__info">
            <a href="mailto:help@brandshop.bog.ge">help@brandshop.bog.ge</a>
            <span>მოგვწერეთ</span>
        </div>
        <!-- /.c-footer__info -->
    </div>
    <!-- /.o-wrapper -->
</footer>
<!-- /.c-footer -->