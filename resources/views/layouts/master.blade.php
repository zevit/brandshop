<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
    <div id="app" class="o-app">
        @include('layouts.partials._header')
        @include('layouts.partials._navigation')

        @if (auth()->guest())
            @include('layouts.partials._login')
        @endif

        <main class="c-content">
            @yield('content')
        </main>
        <!-- /.c-content -->

        @include('layouts.partials._footer')
    </div>
    <!-- /#app -->

    <script src="{{ mix('js/app.js') }}"></script>

    @if($errors->has('email') or $errors->has('password'))
        <script>
            document.querySelector('#login-popup').classList.remove('is-hidden');
        </script>
    @endif
</body>
</html>
