@extends('layouts.master')

@section('content')
    <section class="c-slider">
        <slider :wrap="true" :data="{{ json_encode($slides) }}"></slider>
    </section>
    <!-- /.c-slider -->

    <section class="c-section">
        <div class="o-wrapper">
            <h2 class="c-section__title u-text--uppercase"><em>პოპულარული</em> პროდუქტები</h2>
        </div>
        <!-- /.o-wrapper -->

        <popular-products :data="{{ json_encode($popular) }}"></popular-products>
    </section>
    <!-- /.c-section -->

    <section class="c-section c-section--secondary">
        <featured-products :wrap="true" :data="{{ json_encode($featured) }}"></featured-products>
    </section>
    <!-- /.c-section -->

    <section class="c-section">
        <div class="o-wrapper">
            <h2 class="c-section__title c-section__title--success u-text--uppercase"><em>ბოლოს</em> დამატებული პროდუქცია</h2>

            <latest-products :data="{{ json_encode($latest) }}"></latest-products>

            <div class="c-section__footer u-text--center">
                <a href="{{ route('products') }}" class="o-button c-button c-button--primary u-text--uppercase">მეტი</a>
            </div>
            <!-- /.c-section__footer -->
        </div>
        <!-- /.o-wrapper -->
    </section>
    <!-- /.c-section -->
@endsection
