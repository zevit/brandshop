<div class="c-form__row {{ $errors->has('parent_id') ? 'has-error' : '' }}">
    <label for="parent_id" class="c-form__label">მშობელი:</label>
    <select name="parent_id" id="parent_id" class="c-form__control o-input c-input">
        <option {{ old('parent_id', $category->parent) === null ? 'selected' : '' }} disabled>ძირითადი კატეგორია</option>

        @foreach($categories as $parent)
            <option value="{{ $parent->id }}" {{ old('parent_id', $category->parent_id) === $parent->id ? 'selected' : '' }}>{{ $parent->name }}</option>
        @endforeach
    </select>
    <!-- /.c-form__control -->

    @if($errors->has('parent_id'))
        <span class="c-form__message">{{ $errors->first('parent_id') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="c-form__label">დასახელება:</label>
    <input type="text" name="name" id="name" class="c-form__control o-input c-input" value="{{ $category->name }}" v-geokbd>

    @if($errors->has('name'))
        <span class="c-form__message">{{ $errors->first('name') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="c-form__label">აღწერა:</label>
    <textarea name="description" id="description" class="c-form__control o-input c-input" v-geokbd>{{ old('description', $category->description) }}</textarea>

    @if($errors->has('description'))
        <span class="c-form__message">{{ $errors->first('description') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('image') ? 'has-error' : '' }}">
    <label for="image" class="c-form__label">სურათი:</label>
    <input type="file" name="image" id="image" class="c-form__control o-input c-input o-input--file" accept="image/*" @change="fileSelected">
    <label for="image" class="c-file__chooser">აირჩიეთ სურათი...</label>

    @if($errors->has('image'))
        <span class="c-form__message">{{ $errors->first('image') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('priority') ? 'has-error' : '' }}">
    <label for="priority" class="c-form__label">პრიორიტეტი:</label>
    <input type="number" name="priority" id="priority" class="c-form__control o-input c-input" min="1" value="{{ old('priority', $category->priority ?? 1) }}">

    @if($errors->has('priority'))
        <span class="c-form__message">{{ $errors->first('priority') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row c-form__row--unlabeled {{ $errors->has('is_featured') ? 'has-error' : '' }}">
    <label for="is_featured" class="c-checkbox">
        <input type="checkbox" name="is_featured" id="is_featured" {{ old('is_featured', $category->is_featured ? 'on' : null) === 'on' ? 'checked' : '' }}>
        <span>გამოჩნდეს კატეგორია Featured სექციაში</span>
    </label>

    @if($errors->has('is_featured'))
        <span class="c-form__message">{{ $errors->first('is_featured') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row c-form__row--unlabeled">
    <button type="submit" class="o-button c-button c-button--success c-button--filled u-text--uppercase">შენახვა</button>
</div>
<!-- /.form__row -->
