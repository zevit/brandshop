@extends('admin.layouts.master')

@section('content')
    <div class="o-wrapper u-py-2">
        <div class="u-text--center u-pb-2">
            <a href="{{ route('categories.create') }}" class="o-button c-button c-button--primary u-text--uppercase">
                კატეგორიის დამატება
            </a>
            <!-- /.c-button -->
        </div>
        <!-- /.u-text--center -->

        <div class="c-table__wrapper">
            <table class="c-table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>დასახელება</th>
                        <th>სურათი</th>
                        <th>აღწერა</th>
                        <th>სტატუსი</th>
                        <th><i class="c-icon c-icon--arrow-down"></i></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse($categories as $category)
                        <tr class="c-table__row">
                            <td class="u-text--center">
                                @if ($category->children()->exists())
                                    <a href="#" class="c-table__toggler"
                                       @click.prevent="toggleRow($event, {{ $category->id }})">
                                        <i class="c-icon c-icon--cheveron-right"></i>
                                    </a>
                                @endif
                            </td>
                            <td>{{ $category->name }}</td>
                            <td class="u-text--center">
                                @if($category->hasImage())
                                    <img src="{{ $category->image_path }}" alt="{{ $category->name }}" class="js-zoom" height="50">
                                @endif
                            </td>
                            <td>{{ $category->description}}</td>
                            <td class="u-text--center">
                                {!! ($category->is_featured ? '<span class="u-text--success">FEATURED</span>' : '') !!}
                            </td>
                            <td class="u-text--center">{{ $category->priority}}</td>
                            <td class="c-table__controls u-text--center">
                                <a href="{{ route('categories.edit', $category) }}"
                                   class="o-button o-button--icon c-button"><i class="c-icon c-icon--edit"></i></a>
                                <a href="#" @click.prevent="confirmDelete('{{ route('categories.destroy', $category) }}')"
                                   class="o-button o-button--icon c-button c-button--danger"><i
                                            class="c-icon c-icon--trash"></i></a>
                            </td>
                        </tr>

                        @foreach($category->children as $child)
                            <tr class="c-table__row c-table__row--nested" data-parent="{{ $category->id }}"
                                style="display: none;">
                                <td>&nbsp;</td>
                                <td>{{ $child->name }}</td>
                                <td class="u-text--center">
                                    @if($child->hasImage())
                                        <img src="{{ $child->image_path }}" alt="{{ $child->name }}" class="js-zoom" height="50">
                                    @endif
                                </td>
                                <td>{{ $category->description }}</td>
                                <td class="u-text--center">
                                    {!! ($child->is_featured ? '<span class="u-text--success">FEATURED</span>' : '') !!}
                                </td>
                                <td class="u-text--center">{{ $category->priority}}</td>
                                <td class="c-table__controls u-text--center">
                                    <a href="{{ route('categories.edit', $child) }}"
                                       class="o-button o-button--icon c-button">
                                        <i class="c-icon c-icon--edit"></i>
                                    </a>

                                    <a href="#" @click.prevent="confirmDelete('{{ route('categories.destroy', $child) }}')"
                                       class="o-button o-button--icon c-button c-button--danger">
                                        <i class="c-icon c-icon--trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @empty
                        <tr>
                            <td colspan="6" class="u-text--center">კატეგორია არ არის დამატებული</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <!-- /.c-table -->
        </div>
        <!-- /.c-table__wrapper -->

        {{ $categories->links() }}
    </div>
    <!-- /.o-wrapper -->
@endsection
