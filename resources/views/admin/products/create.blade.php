@extends('admin.layouts.master')

@section('content')
    <div class="o-wrapper u-py-2">
        <div class="u-pb-2">
            <a href="{{ route('products.index') }}" class="o-button c-button u-text--uppercase">უკან</a>
        </div>
        <!-- /.u-text--center -->

        <fieldset class="c-form">
            <legend class="c-form__legend u-text--uppercase">პროდუქტის დამატება</legend>

            <form action="{{ route('products.store') }}" class="c-form__content" method="post"
                  enctype="multipart/form-data">
                @csrf

                @include('admin.products.form')
            </form>
        </fieldset>
    </div>
    <!-- /.o-wrapper -->
@endsection