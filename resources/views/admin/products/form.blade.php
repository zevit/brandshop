<div class="c-form__row {{ $errors->has('categories_id') ? 'has-error' : '' }}">
    <label for="categories_id" class="c-form__label">კატეგორია:</label>
    <select name="categories_id[]" id="categories_id" class="c-form__control o-input c-input" multiple>
        @foreach($categories as $parent)
            <option value="{{ $parent->id }}"
                    {{ in_array($parent->id, old('categories_id', $product->categories->pluck('id')->toArray() ?? [])) ? 'selected' : '' }} class="c-option__group">{{ $parent->name }}</option>

            @foreach($parent->children as $child)
                <option value="{{ $child->id }}"
                        {{ in_array($child->id, old('categories_id', $product->categories->pluck('id')->toArray() ?? [])) ? 'selected' : '' }} class="c-option__child">{{ $child->name }}</option>
            @endforeach
        @endforeach
    </select>
    <!-- /.c-form__control -->

    @if($errors->has('categories_id'))
        <span class="c-form__message">{{ $errors->first('categories_id') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title" class="c-form__label">დასახელება:</label>
    <input type="text" name="title" id="title" class="c-form__control o-input c-input"
           value="{{ old('title', $product->title) }}" v-geokbd>

    @if($errors->has('title'))
        <span class="c-form__message">{{ $errors->first('title') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="c-form__label">აღწერა:</label>
    <textarea name="description" id="description" class="c-form__control o-input c-input js-editor"
              v-geokbd>{{ old('description', $product->description) }}</textarea>

    @if($errors->has('description'))
        <span class="c-form__message">{{ $errors->first('description') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('poster') ? 'has-error' : '' }}">
    <label for="poster" class="c-form__label">პოსტერი:</label>
    <input type="file" name="poster" id="poster" class="c-form__control o-input c-input o-input--file" accept="image/*"
           @change="fileSelected">
    <label for="poster" class="c-file__chooser">აირჩიეთ პოსტერი...</label>

    @if($errors->has('poster'))
        <span class="c-form__message">{{ $errors->first('poster') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('images') ? 'has-error' : '' }}">
    <label for="images" class="c-form__label">სურათები:</label>
    <input type="file" name="images[]" id="images" class="c-form__control o-input c-input o-input--file"
           accept="image/*" multiple data-multiple-caption="არჩეულია {count} სურათი" @change="fileSelected">
    <label for="images" class="c-file__chooser">აირჩიეთ სურათები...</label>

    @if($errors->has('images'))
        <span class="c-form__message">{{ $errors->first('images') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('provider') ? 'has-error' : '' }}">
    <label for="provider" class="c-form__label">მომწოდებელი:</label>
    <input type="text" name="provider" id="provider" class="c-form__control o-input c-input"
           value="{{ old('provider', $product->provider) }}" v-geokbd>

    @if($errors->has('provider'))
        <span class="c-form__message">{{ $errors->first('provider') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('manufacturer') ? 'has-error' : '' }}">
    <label for="manufacturer" class="c-form__label">მწარმოებელი:</label>
    <input type="text" name="manufacturer" id="manufacturer" class="c-form__control o-input c-input"
           value="{{ old('manufacturer', $product->manufacturer) }}" v-geokbd>

    @if($errors->has('manufacturer'))
        <span class="c-form__message">{{ $errors->first('manufacturer') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('price') ? 'has-error' : '' }}">
    <label for="price" class="c-form__label">ფასი:
        <small>(ლარი)</small>
    </label>
    <input type="number" name="price" id="price" class="c-form__control o-input c-input" min="1" step="0.01"
           value="{{ old('price', $product->price ?? 10) }}">

    @if($errors->has('price'))
        <span class="c-form__message">{{ $errors->first('price') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('quantity') ? 'has-error' : '' }}">
    <label for="quantity" class="c-form__label">რაოდენობა:</label>
    <input type="number" name="quantity" id="quantity" class="c-form__control o-input c-input" min="1"
           value="{{ old('quantity', $product->quantity ?? 1) }}">

    @if($errors->has('quantity'))
        <span class="c-form__message">{{ $errors->first('quantity') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('colors') ? 'has-error' : '' }}">
    <label for="colors" class="c-form__label">ფერები:</label>
    <input-tags :tags="{{ json_encode(old('colors', $product->colors ?: [])) }}"
                :items="{{ json_encode(['თეთრი', 'მწვანე', 'ლურჯი', 'წითელი', 'ყვითელი', 'შავი']) }}"></input-tags>

    @if($errors->has('colors'))
        <span class="c-form__message">{{ $errors->first('colors') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('sizes') ? 'has-error' : '' }}">
    <label for="sizes" class="c-form__label">ზომები:</label>
    <input-tags :tags="{{ json_encode(old('sizes', $product->sizes ?: [])) }}"
                :items="{{ json_encode(['XS', 'S', 'M', 'L', 'XL', 'XXL', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47']) }}"></input-tags>

    @if($errors->has('sizes'))
        <span class="c-form__message">{{ $errors->first('sizes') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('tags') ? 'has-error' : '' }}">
    <label for="tags" class="c-form__label">ტეგები:</label>
    <input-tags :tags="{{ json_encode(old('tags', $product->tags->pluck('title')->toArray())) }}"
                :items="{{ json_encode($tags) }}"></input-tags>

    @if($errors->has('tags'))
        <span class="c-form__message">{{ $errors->first('tags') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row c-form__row--unlabeled">
    <button type="submit" class="o-button c-button c-button--success c-button--filled u-text--uppercase">შენახვა
    </button>
</div>
<!-- /.form__row -->
