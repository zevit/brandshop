<form action="{{ route('products.index') }}" class="c-form c-form--horizontal" method="get">
    <div class="c-form__group">
        <input type="text" name="title" id="title" class="c-form__control o-input c-input" value="{{ old('title', request('title')) }}" placeholder="დასახელება">
    </div>
    <!-- /.c-form__group -->

    <div class="c-form__group">
        <select name="category_id" id="category_id" class="c-form__control o-input c-input">
            <option {{ old('category_id', request('category_id')) == null ? 'selected' : '' }} disabled>კატეგორია</option>

            @foreach($categories as $category)
                <optgroup label="{{ $category->name }}">
                    @foreach($category->children as $child)
                        <option value="{{ $child->id }}" {{ $child->id == old('category_id', request('category_id')) ? 'selected' : '' }}>{{ $child->name }}</option>
                    @endforeach

                    <option value="{{ $category->id }}" {{ $category->id == old('category_id', request('category_id')) ? 'selected' : '' }}>ყველა ({{ $category->name }})</option>
                </optgroup>
            @endforeach

            <option value="all" {{ old('category_id', request('category_id')) == 'all' ? 'selected' : '' }}>- ნებისმიერი კატეგორია</option>
        </select>
        <!-- /.c-form__control -->
    </div>
    <!-- /.c-form__group -->

    <div class="c-form__group">
        <select name="rating" id="rating" class="c-form__control o-input c-input" data-old="{{ old('rating', request('rating')) }}">
            <option {{ old('rating', request('rating')) === null ? 'selected' : '' }} disabled>რეიტინგი</option>

            @foreach([1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5] as $rating)
                <option value="{{ $rating }}" {{ $rating == old('rating', request('rating')) ? 'selected' : '' }}>{{ $rating }}</option>
            @endforeach

            <option value="all" {{ old('rating', request('rating')) == 'all' ? 'selected' : '' }}>- ნებისმიერი რეიტინგი</option>
        </select>
        <!-- /.c-form__control -->
    </div>
    <!-- /.c-form__group -->

    <div class="c-form__group">
        <select name="manufacturer" id="manufacturer" class="c-form__control o-input c-input">
            <option {{ old('manufacturer', request('manufacturer')) == null ? 'selected' : '' }} disabled>მწარმოებელი</option>

            @foreach($manufacturers as $manufacturer)
                <option value="{{ $manufacturer }}" {{ $manufacturer === old('manufacturer', request('manufacturer')) ? 'selected' : '' }}>{{ $manufacturer }}</option>
            @endforeach

            <option value="all" {{ old('manufacturer', request('manufacturer')) == 'all' ? 'selected' : '' }}>- ნებისმიერი მწარმოებელი</option>
        </select>
        <!-- /.c-form__control -->
    </div>
    <!-- /.c-form__group -->

    <div class="c-form__group">
        <button class="o-button o-button--small c-button c-button--success c-button--filled">გაფილტვრა</button>
    </div>
    <!-- /.c-form__group -->
</form>