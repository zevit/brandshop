@extends('admin.layouts.master')

@section('content')
    <div class="o-wrapper u-py-2">
        <div class="u-text--center u-pb-2">
            <a href="{{ route('products.create') }}" class="o-button c-button c-button--primary u-text--uppercase">
                პროდუქტის დამატება
            </a>
            <!-- /.c-button -->
        </div>
        <!-- /.u-text--center -->

        <div class="u-mb-1n">
            @include('admin.products.filter')
        </div>
        <!-- /.u-mb-1 -->

        <div class="c-table__wrapper">
            <table class="c-table">
                <thead>
                    <tr>
                        <th>დასახელება</th>
                        <th>კატეგორია</th>
                        <th>პოსტერი</th>
                        <th>ფასი</th>
                        <th>რაოდენობა</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse($products as $product)
                        <tr class="c-table__row">
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->categories->implode('name', ', ') }}</td>
                            <td class="u-text--center">
                                @if($product->hasPoster())
                                    <img src="{{ $product->poster_path }}" alt="Product #{{ $product->id }}" class="js-zoom" height="100">
                                @endif
                            </td>
                            <td class="u-text--right">{{ $product->price }} ₾</td>
                            <td class="u-text--center">{{ $product->quantity }}</td>
                            <td class="c-table__controls u-text--center">
                                <a href="{{ route('products.edit', $product) }}" class="o-button o-button--icon c-button">
                                    <i class="c-icon c-icon--edit"></i>
                                </a>
                                <!-- /.c-button -->

                                <a href="#" @click.prevent="confirmDelete('{{ route('products.destroy', $product) }}')" class="o-button o-button--icon c-button c-button--danger">
                                    <i class="c-icon c-icon--trash"></i>
                                </a>
                                <!-- /.c-button -->
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" class="u-text--center">პროდუქტები არ არის დამატებული</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <!-- /.c-table -->
        </div>
        <!-- /.c-table__wrapper -->

        {{ $products->links() }}
    </div>
    <!-- /.o-wrapper -->
@endsection
