<div class="c-form__row {{ $errors->has('image') ? 'has-error' : '' }}">
    <label for="image" class="c-form__label">სურათი:</label>
    <input type="file" name="image" id="image" class="c-form__control o-input c-input o-input--file" accept="image/*" @change="fileSelected">
    <label for="image" class="c-file__chooser">აირჩიეთ სურათი...</label>

    @if($errors->has('image'))
        <span class="c-form__message">{{ $errors->first('image') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('link') ? 'has-error' : '' }}">
    <label for="link" class="c-form__label">მისამართი (URL):</label>
    <input type="url" name="link" id="link" class="c-form__control o-input c-input" value="{{ $slider->link }}">

    @if($errors->has('link'))
        <span class="c-form__message">{{ $errors->first('link') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row {{ $errors->has('priority') ? 'has-error' : '' }}">
    <label for="priority" class="c-form__label">პრიორიტეტი:</label>
    <input type="number" name="priority" id="priority" class="c-form__control o-input c-input" min="1" value="{{ old('priority', $slider->priority ?? 1) }}">

    @if($errors->has('priority'))
        <span class="c-form__message">{{ $errors->first('priority') }}</span>
    @endif
</div>
<!-- /.form__row -->

<div class="c-form__row c-form__row--unlabeled">
    <button type="submit" class="o-button c-button c-button--success c-button--filled u-text--uppercase">შენახვა</button>
</div>
<!-- /.form__row -->
