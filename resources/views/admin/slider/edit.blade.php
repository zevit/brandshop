@extends('admin.layouts.master')

@section('content')
    <div class="o-wrapper u-py-2">
        <div class="u-pb-2">
            <a href="{{ route('slider.index') }}" class="o-button c-button u-text--uppercase">უკან</a>
        </div>
        <!-- /.u-text--center -->

        <fieldset class="c-form">
            <legend class="c-form__legend u-text--uppercase">სლაიდის რედაქტირება</legend>

            <form action="{{ route('slider.update', $slider) }}" class="c-form__content" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                @include('admin.slider.form')
            </form>
        </fieldset>
    </div>
    <!-- /.o-wrapper -->
@endsection