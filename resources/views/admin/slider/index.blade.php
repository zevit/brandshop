@extends('admin.layouts.master')

@section('content')
    <div class="o-wrapper u-py-2">
        <div class="u-text--center u-pb-2">
            <a href="{{ route('slider.create') }}" class="o-button c-button c-button--primary u-text--uppercase">
                სლაიდის დამატება
            </a>
            <!-- /.c-button -->
        </div>
        <!-- /.u-text--center -->

        <div class="c-table__wrapper">
            <table class="c-table">
                <thead>
                    <tr>
                        <th>სურათი</th>
                        <th>მისამართი (URL)</th>
                        <th><i class="c-icon c-icon--arrow-down"></i></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse($slider as $slide)
                        <tr class="c-table__row">
                            <td class="u-text--center">
                                @if($slide->hasImage())
                                    <img src="{{ $slide->image_path }}" alt="Slide #{{ $slide->id }}" class="js-zoom" height="100">
                                @endif
                            </td>
                            <td>
                                @if($slide->hasLink())
                                    <a href="{{ $slide->link }}" target="_blank">{{ $slide->link}}</a>
                                @endif
                            </td>
                            <td class="u-text--center">{{ $slide->priority }}</td>
                            <td class="c-table__controls u-text--center">
                                <a href="{{ route('slider.edit', $slide) }}" class="o-button o-button--icon c-button">
                                    <i class="c-icon c-icon--edit"></i>
                                </a>
                                <!-- /.c-button -->

                                <a href="#" @click.prevent="confirmDelete('{{ route('slider.destroy', $slide) }}')"
                                   class="o-button o-button--icon c-button c-button--danger">
                                    <i class="c-icon c-icon--trash"></i>
                                </a>
                                <!-- /.c-button -->
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="u-text--center">სლაიდერი არ არის დამატებული</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <!-- /.c-table -->
        </div>
        <!-- /.c-table__wrapper -->

        {{ $slider->links() }}
    </div>
    <!-- /.o-wrapper -->
@endsection
