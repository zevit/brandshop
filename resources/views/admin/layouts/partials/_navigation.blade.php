<nav class="c-navigation">
    <div class="o-wrapper">
        <ul class="c-navigation__list">
            <li class="c-navigation__item">
                <a class="c-navigation__link u-text--uppercase {{ request()->is('admin/categories*') ? 'is-active' : '' }}" href="{{ route('categories.index') }}">
                    კატეგორიები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.c-navigation__item -->

            <li class="c-navigation__item">
                <a class="c-navigation__link u-text--uppercase {{ request()->is('admin/slider*') ? 'is-active' : '' }}" href="{{ route('slider.index') }}">
                    სლაიდერი
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.c-navigation__item -->

            <li class="c-navigation__item">
                <a class="c-navigation__link u-text--uppercase {{ request()->is('admin/products*') ? 'is-active' : '' }}" href="{{ route('products.index') }}">
                    პროდუქცია
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.c-navigation__item -->

            <li class="c-navigation__item">
                <a class="c-navigation__link u-text--uppercase {{ request()->is('admin/transactions*') ? 'is-active' : '' }}" href="#">
                    ტრანზაქციები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.c-navigation__item -->

            <li class="c-navigation__item">
                <a class="c-navigation__link u-text--uppercase {{ request()->is('admin/purchases*') ? 'is-active' : '' }}" href="#">
                    შეკვეთები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.c-navigation__item -->
        </ul>
        <!-- /.c-navigation__list -->
    </div>
    <!-- /.o-wrapper -->
</nav>
<!-- /.c-navigation -->