<footer class="c-footer c-footer--small">
    <div class="o-wrapper u-text--center">
        <span class="u-text--muted">შექმნილია 💛️-ით "საქართველოს ბანკი"-სთვის</span>
    </div>
    <!-- /.o-wrapper -->
</footer>
<!-- /.c-footer -->