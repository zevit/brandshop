<header class="c-header">
    <div class="o-wrapper">
        <a href="{{ url('/') }}" class="c-logo">Dashboard</a>

        <div class="c-header__controls">
            <button class="o-button c-button u-text--uppercase">გასვლა</button>
        </div>
        <!-- /.c-header__controls -->
    </div>
    <!-- /.o-wrapper -->
</header>
<!-- /.c-header -->