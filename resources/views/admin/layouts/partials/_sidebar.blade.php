<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin') ? 'active' : '' }}" href="{{ route('dashboard') }}">
                    <i class="c-icon c-icon--home"></i>
                    სამართავი პანელი
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->

            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/categories') ? 'active' : '' }}" href="{{ route('categories') }}">
                    <i class="c-icon c-icon--folder"></i>
                    კატეგორიები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->

            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/brands') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--award"></i>
                    ბრენდები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->

            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/slider') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--film"></i>
                    სლაიდერი
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->
        </ul>
        <!-- /.nav -->

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>პროდუქცია</span>
        </h6>
        <!-- /.sidebar-heading -->

        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/featured') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--star"></i>
                    გამორჩეული პროდუქცია
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->

            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/products') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--package"></i>
                    პროდუქციის მართვა
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->
        </ul>
        <!-- /.nav -->

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>ფინანსური ოპერაციები</span>
        </h6>
        <!-- /.sidebar-heading -->

        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/transactions') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--credit-card"></i>
                    ტრანზაქციები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->

            <li class="nav-item">
                <a class="nav-link {{ request()->is('admin/products') ? 'active' : '' }}" href="#">
                    <i class="c-icon c-icon--shopping-bag"></i>
                    შეკვეთები
                </a>
                <!-- /.nav-link -->
            </li>
            <!-- /.nav-item -->
        </ul>
        <!-- /.nav -->
    </div>
    <!-- /.sidebar-sticky -->
</nav>
<!-- /.sidebar -->