<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <title>{{ config('app.name') }} | სამართავი პანელი</title>

    <script>
        window.Global = {
            token: '{{ csrf_token() }}'
        };
    </script>
</head>
<body>
    <div id="app" class="o-app">
        @include('admin.layouts.partials._header')
        @include('admin.layouts.partials._navigation')

        <main class="c-content">
            @yield('content')
        </main>
        <!-- /.c-content -->

        @include('admin.layouts.partials._footer')
    </div>
    <!-- /#app -->

    <script src="{{ mix('js/admin.js') }}"></script>

    @if(session()->has('success'))
        <script>window.notif.confirm('{{ session('success') }}');</script>
    @endif

    @if(session()->has('danger'))
        <script>window.notif.alert('{{ session('danger') }}');</script>
    @endif

    @stack('script')
</body>
</html>