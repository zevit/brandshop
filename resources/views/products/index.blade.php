@extends('layouts.master')

@section('content')
    <div class="o-wrapper">
        @if (isset($category))
            <products-page category="{{ $category->slug }}"></products-page>
        @else
            <products-page search="{{ request('query') }}"></products-page>
        @endif
    </div>
    <!-- /.o-wrapper -->
@endsection
