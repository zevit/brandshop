@extends('layouts.master')

@section('content')
    <div class="o-wrapper">
        <div class="c-details">
            <div class="c-details__header">
                <a href="#"><i class="icon icon-back"></i> უკან კატეგორიის გვერდზე "ტექნიკა"</a>
            </div>
            <!-- /.c-section__header -->

            <div class="c-details__content">
                <product-page :data="{{ json_encode($product) }}"></product-page>

                <div class="c-details__related">
                    <h2 class="c-details__title">მსგავსი პროდუქცია</h2>

                    <related-products :product="{{ $product->id }}"></related-products>
                </div>
                <!-- /.c-details__related -->
            </div>
            <!-- /.c-details__content -->
        </div>
        <!-- /.c-section -->
    </div>
    <!-- /.o-wrapper -->
@endsection
