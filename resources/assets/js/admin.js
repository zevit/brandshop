window.Vue = require('vue');

window.tingle = require('tingle.js');

window.$ = window.jQuery = require('jquery');

require('trumbowyg');

const Lightense = require('lightense-images');

const Notyf = require('notyf');

import geokbd from 'vue-geokbd';

Vue.use(geokbd, {enabled: true});

window.addEventListener('load', function () {
    Lightense('img.js-zoom');
}, false);

window.notif = new Notyf({
    delay: 2000,
    alertIcon: 'c-icon c-icon--x-circle',
    confirmIcon: 'c-icon c-icon--check-circle'
});

$(document).ready(() => {
    $.trumbowyg.svgPath = '/images/icons.svg';

    $('.js-editor').trumbowyg({
        btns: [
            ['viewHTML'],
            ['undo', 'redo'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['link'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        autogrow: true,
        removeformatPasted: true
    });
});

// Inputs
Vue.component('input-tags', require('./components/InputTags.vue'));

const app = new Vue({
    el: '#app',
    methods: {
        toggleRow(e, parent) {
            const parentColumn = e.target.parentNode.parentNode;
            const parentRow = parentColumn.parentNode;
            const children = document.querySelectorAll('.c-table__row--nested');


            parentRow.classList.toggle('is-opened');
            parentColumn.querySelector('.c-icon').classList.toggle('c-icon--cheveron-right');
            parentColumn.querySelector('.c-icon').classList.toggle('c-icon--cheveron-down');

            children.forEach(child => {
                child.style.display = 'none';

                if (+child.dataset.parent === parent && parentRow.classList.contains('is-opened')) {
                    child.style.display = '';
                }
            });
        },

        fileSelected(e) {
            const label = e.target.nextElementSibling;
            const labelValue = label.innerHTML;
            let fileName = e.target.value.split('\\').pop();

            if (e.target.files && e.target.files.length > 1) {
                fileName = (e.target.dataset.multipleCaption || '').replace('{count}', e.target.files.length);
            }

            label.innerHTML = fileName ? fileName : labelValue;
        },

        confirmDelete(action) {
            const modal = new window.tingle.modal({
                closeMethods: [],
                footer: true,
                stickyFooter: true
            });

            const formId = 'delete_' + Date.now();

            modal.setContent([
                '<form id="' + formId + '" action="' + action + '" method="post" style="display: none;">',
                '<input type="hidden" name="_method" value="DELETE">',
                '<input type="hidden" name="_token" value="' + window.Global.token + '">',
                '<button type="submit">წაშლა</button>',
                '</form>',
                '<p>ნამდვილად გსურთ წაშლა?</p>'
            ].join("\n"));

            modal.addFooterBtn('დიახ', 'tingle-btn tingle-btn--primary tingle-btn--pull-right', () => {
                document.getElementById(formId).submit();
                modal.close();
            });

            modal.addFooterBtn('არა', 'tingle-btn tingle-btn--default tingle-btn--pull-right', () => {
                modal.close();
            });

            modal.open();
        }
    }
});
