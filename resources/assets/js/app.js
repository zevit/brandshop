/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// Vue.js
window.Vue = require('vue');

const VueTouch = require('vue-touch/dist/vue-touch');
Vue.use(VueTouch, {name: 'v-touch'});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Products
Vue.component('popular-products', require('./components/PopularProducts.vue'));
Vue.component('featured-products', require('./components/FeaturedProducts.vue'));
Vue.component('latest-products', require('./components/LatestProducts.vue'));
Vue.component('latest-products', require('./components/LatestProducts.vue'));
Vue.component('related-products', require('./components/RelatedProducts.vue'));

// Cart
Vue.component('cart', require('./components/Cart.vue'));
Vue.component('cart-item', require('./components/CartItem.vue'));
Vue.component('cart-header', require('./components/CartHeader.vue'));
Vue.component('cart-details', require('./components/CartDetails.vue'));
Vue.component('cart-history', require('./components/CartHistory.vue'));

// Filters
Vue.component('filters', require('./components/Filters.vue'));
Vue.component('filter-list', require('./components/FilterList.vue'));

// Misc
Vue.component('search', require('./components/Search.vue'));
Vue.component('rating', require('./components/Rating.vue'));
Vue.component('slider', require('./components/Slider.vue'));
Vue.component('link-form', require('./components/LinkForm.vue'));
Vue.component('pagination', require('./components/Pagination.vue'));

// Pages
Vue.component('products-page', require('./pages/Products.vue'));
Vue.component('product-page', require('./pages/Product.vue'));
Vue.component('cart-page', require('./pages/Cart.vue'));

const app = new Vue({
    el: '#app',
    created() {
        key('esc', () => {
            this.closeLogin(null, true);
        });
    },
    methods: {
        showLogin() {
            document.querySelector('#login-popup').classList.remove('is-hidden');
        },

        closeLogin(e, force) {
            if ((e && e.target.id === 'login-popup') || force) {
                document.querySelector('#login-popup').classList.add('is-hidden');
            }
        },
    }
});
